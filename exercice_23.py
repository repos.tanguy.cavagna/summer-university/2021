from exercice_22 import count_words2
import operator


def max_val(dic: dict) -> str:
    return max(dic.items(), key=operator.itemgetter(1))[0]


if __name__ == "__main__":
    print(
        max_val(
            count_words2(
                "Beautiful is better than ugly. Explicit is better than implicit. Simple is better than complex. Complex is better than complicated."
            )
        )
    )

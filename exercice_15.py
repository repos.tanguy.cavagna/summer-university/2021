from typing import List


activities = {
    "Mathilde": {1, 3, 4, 5, 10},
    "Jean-Claude": {2, 5, 7, 8, 10},
    "Gustave": {4, 8, 10}
}


def common_activities(act: dict) -> set:
    interset = set(list(act.values())[0]).intersection(*list(act.values())[1:])
    return interset


def as_been_chose(act: dict, act_id: int) -> List[str]:
    return [k for k, v in act.items() if act_id in v]


if __name__ == "__main__":
    print(common_activities(activities))
    print(as_been_chose(activities, 12))

## Exercice 1

Ecrivez les fonctions suivantes:

* `f(x) = 3*x+2`, de signature: `(num) -> (num)`
* `g(x) = x^2`, de signature: `(num) -> (num)`
* `h(x, y) = x + 2 * y`, de signature: `(num, num) -> (num)`
* `i(x) = "x"`, de signature: `(num) -> (str)`
* `j(x) = f(g(x)) =  (f o g)(x)`, de signature: `(num) -> (num)`
* `k(x, y) = i(h(x,y)) = (i o h)(x,y)`, de signature: `(num, num) -> (str)`

## Exercice 2

*  Ecrivez une fonction `strictement_positif(a)` indiquant si un entier est strictement positif.  Opérateurs autorisés : `<, >, >=, <=`.
*  Ecrivez une fonction `maximum(a, b)` prenant en paramètre deux entiers et retournant la valeur du plus grand des deux. Opérateurs  autorisés : `==, <, >, <=, >=`.
*  A l'aide de la fonction précédente, écrivez une fonction `minimum(a, b)` permettant de retourner la valeur minimum de deux entiers. Opérateurs autorisés : `==, !=`.
*  A l'aide de la fonction précédente, écrivez une fonction `negatif(a) ` indiquant si un entier est négatif ou nul. Opérateurs autorisés : `==, !=`.
*  Ecrivez une fonction `pair(a)` indiquant si un entier est pair.  Opérateurs autorisés : `%, ==, !=`.
*  Proposez une fonction permettant de retourner l'opposé d'un nombre. (Exemple : l'opposé de 5 est -5, celui de -3 est 3)
*  A l'aide des fonctions `negatif` et `oppose`, écrivez une fonction qui retourne la valeur absolue d'un nombre

## Exercice 3

Réalisez l'algorithme représentant la fonction ci-dessous sans utiliser de branchement conditionnel (utilisez les fonctions de l'exercice 2):

# Boucles et structures de données

## Exercice 4

Réaliser une proécdure qui affiche une table de multiplication. Elle prend en paramètre la valeur maximum des multiples. Par exemple, l'appel de la procédure `table_mult(4)` affichera le résultat ci-dessous. Le multiple minimum est 2.


2  *  2  =  4
2  *  3  =  6
2  *  4  =  8
3  *  2  =  6
3  *  3  =  9
3  *  4  =  12
4  *  2  =  8
4  *  3  =  12
4  *  4  =  16


## Exercice 5

A l'aide d'un paramètre `n`, écrivez une fonction `afficher_sapin(n)` qui affiche la suite de symboles sous cette forme (exemple avec `n=5`):


        *
       ***
      *****
     *******
    *********

## Exercice 6

Ecrivez une fonction `demander_valeur_entiere()` qui demande à l'utilisateur une valeur entière  et la retourne. Cette fonction redemande à l'utilisateur d'entrer une valeur tant que celle-ci n'est pas une valeur entière. Il est possible de passer en argument de la fonction le texte à afficher à l'utilisateur. Aidez-vous de la méthode `isnumeric()` qui s'applique sur les chaînes de caractères.

Exemples :

*  `"2".isnumeric()` retourne `True`
*  `"2sd".isnumeric()` retourne `False`
*  `"-2".isnumeric()` retourne `False`

Vous remarquerez que la méthode `isnumeric()` ne fonctionne qu'avec des valeurs positives ou nulles. Trouvez une solution pour que votre fonction puisse s'appliquer avec des valeurs négatives.

Exemple de fonctionnement :

    Entrez une valeur entière : asdf
    Valeur erronée
    Entrez une valeur entière : 3.8
    Valeur erronée
    Entrez une valeur entière : -4
    La valeur retournée est -4


## Exercice 7

A l'aide de la fonction précédente, écrivez deux fonctions:
*  une première qui demande une valeur entière supérieure ou égale à une borne,
*  une seconde qui demande une valeur entière entre une borne min et max.

## Exercice 8

La suite de Fibonacci est une suite d'entiers dans laquelle chaque terme est la somme des deux termes qui le précèdent. L'algorithme commence avec les nombre 0 et 1. Après 10 itérations on obtient la suite suivante : 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55.

## Exercice 9

Ecrivez la fonction `est_numerique(s)` de signature `str->bool` qui précise si l’argument `s`, de type `str`, est numérique ou non (`float` ou `int`). Voici un exemple d’utilisation de cette fonction :

*  `est_numerique("adsf")` retourne `False`
*  `est_numerique("-23.2")` retourne `True`
*  `est_numerique("-3dfdf")` retourne `False`
*  `est_numerique("44")` retourne `True`

# Slicing

## Exercice 10

Réalisez un programme qui demande une chaîne de caractères à l'utilisateur et affiche :

*  `True` si les lettres de la chaîne sont en majuscules
*  Le nombre d'occurrences de la première lettre dans la chaîne
*  Le nombre d'occurrences de la dernière lettre dans la chaîne
*  `True` si la première et dernière lettre sont identiques


## Exercice 11

Un palindrome est un mot ou une phrase qui signifie la même chose si on le lit de gauche à droite ou de droite à gauche. Quelques exemples de palindromes : kayak, mon nom, à, été, selles, sexes, tôt, ...

Réalisez un programme retournant `True` si le mot ou la phrase entrée par l'utilisateur est un palindrome, `False` sinon.

## Exercice 12

A l’aide de la fonction estnumerique(), écrivez une fonction `double` qui prend une liste de chaînes de caractères et retourne une liste de `float` où tous les éléments de la liste sont multipliés par deux s’ils sont numériques. Les éléments non numériques ne sont pas retournés.
Exemple d’utilisation:

*  `double(["-2", "5.0", "7"])` retourne `[-4.0, 10.0, 14.0]`
*  `double(["saaf", "-3", "4.0", "coucou"])` retourne `[-6.0, 8.0]`
*  `double([])` retourne `[]`
*  `double(["aaa"])` retourne `[]`


## Exercice 13

A l’aide le la fonction `double` et de la fonction `len`, écrivez une fonction `verifier` qui précise si tous les éléments d’une liste sont numériques.
Exemple d’utilisation:

*  `verifier([])` retourne `True`
*  `verifier(["-2", "14", "18.32"])` retourne `True`
*  `verifier(["coucou", "-5", "3"])` retourne `False`
*  `verifier(["aaa", "bbb", "-3"])` retourne `False`

La fonction `len` existe en python. Elle prend une liste et retourne la taille de la liste.
Exemple d’utilisation:

*  `len([])` retourne `0`
*  `len([1, 6, 7])` retourne `3`
*  `len([44] )` retourne `1`

## Exercice 14

Réalisez un script comprenant un ensemble de fonctionnalités.

### longueur
Cette fonction prend une liste en paramètre et retourne sa longueur. Par exemple, `longueur([1,5,4])` retourne 3. N'utilisez pas la fonction `len` de Python qui réalise la même fonctionnalité.

### moyenne
Cette fonction prend une liste en paramètre et retourne sa moyenne. Par exemple, `moyenne([4,5,3])` retourne 4.0.

### plus grand ou égal
Cette fonction prend une liste en paramètre et un seuil et retourne une nouvelle liste avec les éléments plus grands ou égaux au seuil. Par exemple, `plus_grand_ou_egal([4,5,12, 9, 3], 7)` retourne [12, 9].

### plus petit ou égal
Cette fonction prend une liste en paramètre et un seuil et retourne une nouvelle liste avec les éléments plus petits ou égaux au seuil. Par exemple, `plus_petit_ou_egal([4,5,12, 9, 3], 9)` retourne [4, 5, 9, 3].

### positive
Cette fonction prend une liste en paramètre et retourne une nouvelle liste avec les éléments positifs ou nuls. Par exemple, `positive([4, -2, 3, 1])` retourne [4, 3, 1].

### négative
Cette fonction prend une liste en paramètre et retourne une nouvelle liste avec les éléments négatifs ou nuls. Par exemple, `negative([4, -2, 3, 1])` retourne [-2].

### dans
Cette fonction prend une liste et un nombre et vérifie si ce nombre se trouve dans la liste. N'utilisez pas l'opérateur `in`. Par exemple, `dans([1,2,3], 3)` retourne True.

### minimum
Cette fonction prend une liste et retourne l'élément le plus petit de la liste. Par exemple, `minimum([3,2,1,6])` retourne 1. N'utilisez pas la fonction `min` de Python qui réalise la même fonctionnalité.

### maximum
Cette fonction prend une liste et retourne l'élément le plus grand de la liste. Par exemple, `minimum([3,2,1,6])` retourne 6. N'utilisez pas la fonction `max` de Python qui réalise la même fonctionnalité.

### répéter
Cette procédure affiche $n$ fois une chaîne de caractères entrée en paramètre. N'utilisez pas l'opérateur `*` de Python.

### binaire
Cette fonction prend un nombre binaire, représenté par une chaîne de caractères de '0' et de '1', et retourne la valeur décimale. On suppose que la chaîne de caractères est valide. Par exemple, `binaire("0110")` retourne 6.

### and lst
Cette fonction prend une liste de valeur booléenne en paramètre et retourne sa réduction à l'aide de l'opération ET logique. Par exemple,  `and_lst([True, True, False])` retourne False car l'expression `True and True and False` retourne False

### or lst
Cette fonction prend une liste de valeur booléenne en paramètre et retourne sa réduction  à l'aide de l'opération OU logique. Par exemple,  `or_lst([True, True, False])` retourne True car l'expression `True or True or False` retourne True

### zip
Cette fonction 'fermeture éclaire' prend deux listes et retourne une liste de tuples ou chaque tuple est une composition d'élément des deux listes. Si les deux listes ne sont pas de même taille alors il faut retourner une liste vide. Par exemple, `zip([1,2,3], ["a", "b", "c"])` retourne [(1, "a"), (2, "b"), (3, "c")].

### unzip
Cette fonction réalise l'opération inverse. Elle prend une liste de tuple et retourne deux listes. Par exemple, `unzip([(1, "a"), (2, "b"), (3, "c"))` retourne [1,2,3], ["a", "b", "c"].

### transform
Cette fonction prend une liste et une fonction f en paramètre et retourne une nouvelle liste où la fonction f est appliquée sur chaque élément. Par exemple:

`def f(x):      `
`  return 2*x+2 `

transform([1,2,3], f) # retourne [4, 6, 8]


### filter
Cette fonction prend une liste et un prédicat et retourne une nouvelle liste où chaque élément vérifie ce prédicat. Par exemple :

def majeur(age):
  return age >= 18

filter([12,21,13, 45], majeur) # retourne [21, 45]

## Ensembles

### Duplicate (exercice 15)
Ecrivez une fonction `remove_duplicates(lst)` qui prend en paramètre une liste et qui retourne une nouvelle liste ne contenant aucun élément dupliqué. L'ordre ne doit pas forcément être respecté.

* Entrée: une liste contenant des entiers, des caractères ou des chaînes de caractères.
* Sortie: une liste dont chaque élément n'apparaît qu'une fois

Exemple : `remove_duplicates([1,2,2,'a','a'])` retournera une liste similaire à `[1,2,'a']`


### Activités (exercice 16)
Trois personnes (Mathilde, Jean-Claude et Gustave)  ont choisi des activités. Chaque activité possède un numéro unique allant de 1 à n.
* Quelle est l'opération permettant de connaître les activités communes à ces trois personnes ?
* Quelle est l'opération permettant de connaître si une activité lambda a été choisie par au moins une personne ?

Aide: Les choix de chaque personne peuvent être représentés par un ensemble. Mathilde = {1,3,4,5} signifie que que Mathilde a choisi les activité 1, 3, 4 et 5.

## Dictionnaires 

### My dico (exercice 17)
Un dictionnaire en Python peut être vu comme une liste de tuple.
Réalisez un module qui met à disposition un ensemble de fonctionnalités permettant la gestion
d’un dictionnaire. Utilisez un style purement impératif.

dictionnaire = []
add(dictionnaire, "nom", "Adrien")
add(dictionnaire, "age", 20) # dictionnaire = [("nom", "Adrien"), ("age", 20)]
val = drop(dictionnaire, "age") #val = 20, dictionnaire = [("nom", "Adrien")]

Réalisez les fonctions suivantes :

— index(dic, v) : retourne la première clé trouvée associée à la valeur v.
— is_in(dic, k) : vérifie si la clé se trouve dans le dictionnaire
— add(dic, k, v) : ajoute un élément dans le dictionnaire. Remplace la valeur v si k existe déjà
— get(dic, k) : retourne la valeur correspondant à une clé
— get_or_else(dic, k, v) : retourne v si la clé k n’existe pas
— length(dic) : retourne la longeur du dictionnaire
— drop(dic, k) : supprime le couple clé/valeur du dictionnaire et retourne la valeur associée
— values(dic) : retourne toutes les valeurs du dictionnaire
— keys(dic) : retourne toutes les clés du dictionnaire
— reverse(dic) : retourne un dictionnaire ou les clés et les valeurs sont inversées
Créez vos propres exceptions pour gérer les différents problèmes que l’on pourrait rencontrer :


class KeyException(Exception): pass

# Compréhension de liste

## Exercice 18
A l'aide de compréhension de liste, écrivez les fonctions ci-dessous :
* positive(xs): retourne tous les éléments positifs dans la liste xs
* simplify(xs): retourne le premier élément de chaque tuple dans la liste xs (exemple: simplify( [(1,2), (3,4)] ) retourne [1,3]
* negate(xs): retourne l'opposé de chaque nombre de la liste xs si l'élément est pair
* dico(xs): retourne un dictionnaire à l'aide d'une liste de tuple entré en paramètre (exemple: dico( [("id",2), ("age",4)] ) retourne [{"id": 2}, {"age": 4}]
* to_int(str): Liste de string en liste d'integer
* first_char(str): 1ere lettre de chaque mot. "Je suis une phrase" retourne ['J', 's', 'u', 'p']
* longer_than(str): Chaque mot de plus de 3 caractères
* space_count(str): Compter le nombre d'espace dans une chaine de caractères
* word_size(str): Prend une chaine de caractère comprenant une phrase et retourne un dictionnaire avec comme clé un mot et comme valeur la taille du mot.

# Dictionnaires

## squares (exercice 19)
Cette fonction génère un dictionnaire de toutes les carrés de 1 à n, avec comme clé n et comme valeur n au carré. Par exemple `squares(3)` retourne {1: 1, 2: 4, 3: 9}

## words to ascii (exercice 29)
Cette fonction `words_to_ascii` prend en entrée une liste de string et retourne un dictionnaire avec comme clé chaque string et comme valeur une liste des valeurs ascii de chaque caractère qui compose la clé. Par exemple, `words_to_ascii(["Encore", "10", "mois", "à", "tenir"])` retourne

{'10': [49, 48],
 'Encore': [69, 110, 99, 111, 114, 101],
 'mois': [109, 111, 105, 115],
 'tenir': [116, 101, 110, 105, 114],
 'à': [224]}


La fonction `ord` retourne la valeur  `ASCII` d'un caractère.

## avg values (exercice 21)

Cette fonction `avg_values` retourne un dictionnaire et calcule, pour chaque clé, sa valeur moyenne. Par exemple, `avg_values(words_to_ascii(["Encore", "10", "mois", "à", "tenir"]))` retourne

{'10': 48.5,
 'Encore': 100.66666666666667,
 'mois': 110.0,
 'tenir': 109.2,
 'à': 224.0}

## count words (exercice 22)
Cette fonction `count_words` prend un string en entrée et retourne le nombre d'occurrences de chaque mot, sans prendre en compte la ponctuation.
Par exemple `count_words("Beautiful is better than ugly. Explicit is better than implicit. Simple is better than complex. Complex is better than complicated.")` retourne

{'beautiful': 1,
 'better': 4,
 'complex': 2,
 'complicated': 1,
 'explicit': 1,
 'implicit': 1,
 'is': 4,
 'simple': 1,
 'than': 4,
 'ugly': 1}


## max val (exercice 23)
Cette fonction `max_val(dic)` recherche la valeur maximum du dictionnaire et retourne la clé associée. Si le dictionnaire possède plusieurs valeurs max, la fonction en retourne une seule choisie arbitrairement. Par exemple, avec `words` le résultat de la fonction `count_words` précédente, `max_val(words)` retourne `is`


## sort by value (exercice 24)
Cette procédure `sort_by_value(dic)` affiche le dictionnaire trié par valeurs décroissantes. Par exemple, avec `words` le résultat de la fonction `count_words` précédente, `sort_dict(words)` retourne

than 4
better 4
is 4
complex 2
complicated 1
simple 1
implicit 1
explicit 1
ugly 1
beautiful 1

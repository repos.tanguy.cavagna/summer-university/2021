from typing import List
from exercice_9 import is_numeric


def double(lst: List[str]) -> List[float]:
    return [float(i) * 2 for i in lst if is_numeric(i)]


if __name__ == "__main__":
    print(double(["aaa"]))

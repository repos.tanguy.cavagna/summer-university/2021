import re


def count_words2(foo: str) -> dict:
    return {w: foo.count(w) for w in re.sub(r"[\.\-?!_:;]", "", foo).split(" ")}


if __name__ == "__main__":
    print(
        count_words2(
            "Beautiful is better than ugly. Explicit is better than implicit. Simple is better than complex. Complex is better than complicated."
        )
    )

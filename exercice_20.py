def word_to_ascii(foo: list[str]) -> dict:
    return {w: [ord(c) for c in w] for w in foo}


if __name__ == "__main__":
    print(word_to_ascii(["Encore", "10", "mois", "à", "tenir"]))

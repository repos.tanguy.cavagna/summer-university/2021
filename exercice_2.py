def strictly_positive(a: int) -> bool:
    return a > 0


def maximum(a: int, b: int) -> int:
    return a if (a > b) else b


def minimum(a: int, b: int) -> int:
    return a if maximum(a, b) != a else b


def is_negative(a: int) -> bool:
    return minimum(a, 0) == a


def pair(a: int) -> bool:
    return a % 2 == 0


def oposite(a: int) -> int:
    return -a


def absolute(a: int) -> int:
    return a if not is_negative(a) else oposite(a)

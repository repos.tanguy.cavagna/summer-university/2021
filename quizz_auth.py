from getpass import getpass

ACCOUNTS = set({("admin", "secret"), ("user", "password")})


def isAuthorized(login: str, pwd: str) -> bool:
    return (login, pwd) in ACCOUNTS


login = input("Nom d'utilisateur : ")
pwd = getpass("Mot de passe : ")

if isAuthorized(login, pwd):
    print(f"Connecté en tant que {login}")
else:
    print("Connection echouée")

def is_palindrom(s: str) -> bool:
    lhs, rhs = (s[:len(s) // 2], s[len(s) // 2:])
    rhs = rhs[::-1] if (len(rhs) == len(lhs)) else rhs[::-1][:-1]
    return lhs == rhs


if __name__ == "__main__":
    print(is_palindrom("yadhay"))

import operator
from exercice_22 import count_words2


def sort_by_value(dic: dict) -> dict:
    return dict(reversed(sorted(dic.items(), key=operator.itemgetter(1))))


if __name__ == "__main__":
    print(
        sort_by_value(
            count_words2(
                "Beautiful is better than ugly. Explicit is better than implicit. Simple is better than complex. Complex is better than complicated."
            )
        )
    )

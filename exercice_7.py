from exercice_6 import ask_integer_value


def ask_goe_value(min_limit: int) -> int:
    while value := ask_integer_value():
        if value < min_limit:
            print(f"La valeur doit être >= {min_limit}")
            continue
        break
    return value


def ask_in_between(min_: int, max_: int) -> int:
    while value := ask_goe_value(
            min_limit=min_):
        if value > max_:
            print(f"La valeur doit être <= {max_}")
            continue
        break
    return value


if __name__ == "__main__":
    print(ask_in_between(4, 9))

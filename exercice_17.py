from typing import Any


class KeyException(Exception):
    ...


def add(dictionnary: list, k: Any, v: Any):
    dictionnary.append((k, v))


def drop(dictionnary: list, k: Any) -> Any:
    for idx, (_k, _) in enumerate(dictionnary):
        if k == _k:
            dictionnary.pop(idx)
            return _k
    raise KeyException("Key not found")


def index(dictionnary: list, v: Any) -> Any:
    return next((_k for _k, _v in dictionnary if _v == v), None)


def is_in(dictionnary: list, k: Any) -> bool:
    found = next((v for _k, v in dictionnary if k == _k), None) is not None
    if not found:
        raise KeyException("Key not found")
    return found


def get(dictionnary: list, k: Any) -> Any:
    if is_in(dictionnary, k):
        return next((_v for _k, _v in dictionnary if k == _k))


def get_or_else(dictionnary, k: Any, v: Any) -> Any:
    try:
        return get(dictionnary, k)
    except KeyException:
        return v


def length(dictionnary: list) -> int:
    return len(dictionnary)


def values(dictionnary: list) -> list:
    return [_v for _, _v in dictionnary]


def keys(dictionnary: list) -> list:
    return [_k for _k, _ in dictionnary]


def reverse(dictionnary: list) -> list:
    return [(_v, _k) for _k, _v in dictionnary]


if __name__ == "__main__":
    dictionnary = []
    add(dictionnary, "nom", "Adrien")
    add(dictionnary, "age", 20)
    print(index(dictionnary, 20))
    print(is_in(dictionnary, "age"))
    print(get(dictionnary, "nom"))
    print(get_or_else(dictionnary, "nom2", "Pas trouvé"))
    print(length(dictionnary))
    print(values(dictionnary))
    print(keys(dictionnary))
    print(reverse(dictionnary))
    val = drop(dictionnary, "age")
    print(val, dictionnary)

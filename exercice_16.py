from typing import List, Any


def remove_duplicates(lst: List[Any]) -> List[Any]:
    res = []
    for e in lst:
        if e not in res:
            res.append(e)
    return res


if __name__ == "__main__":
    print(remove_duplicates([1, 2, 2, "a", "a"]))

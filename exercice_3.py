from exercice_2 import is_negative


def graph(x: int) -> int:
    # PLUS SIMPLE => return maximum(2, x)
    return (x * (not is_negative(x))) + (2 * is_negative(x))

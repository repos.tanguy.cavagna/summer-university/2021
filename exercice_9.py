import re


def is_numeric(s: str) -> bool:
    return re.match(r"^[+-]?(\d+(?:[\.]\d+)?|\.\d+)$", s) is not None

# from statistics import mean
from exercice_20 import word_to_ascii


def avg_values(dic: dict) -> dict:
    # return {k: mean(v) for k, v in dic}
    avg = lambda x: sum(x) / len(x)
    return {k: avg(v) for k, v in dic.items()}


if __name__ == "__main__":
    print(avg_values(word_to_ascii(["Encore", "10", "mois", "à", "tenir"])))

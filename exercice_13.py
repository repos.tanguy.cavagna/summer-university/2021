from typing import List
from exercice_12 import double


def verify(lst: List[str]) -> bool:
    return (len(lst) == len(double(lst)))


if __name__ == "__main__":
    print(verify(["coucou", "aaaa", "3"]))

def pine(x: int) -> str:
    return "\n".join([" " * (x - i - 1) + ("*" * (2 * i + 1)) for i in range(x)])


if __name__ == "__main__":
    print(pine(5))

def positive(xs: list[int]) -> list[int]:
    return [i for i in xs if i > 0]


def simplify(xs: list[tuple]) -> list[int]:
    return [i for i, _ in xs]


def negate(xs: list[int]) -> list[int]:
    return [-i for i in xs if i % 2 == 0]


def dico(xs: list[tuple]) -> list[dict]:
    return [{_k: _v} for _k, _v in xs]


def to_str(xs: list[str]) -> list[int]:
    return [int(s) for s in xs]


def first_char(foo: str) -> list[str]:
    return [s[0] for s in foo.split(" ")]


def longer_than(foo: str) -> list[str]:
    return [s for s in foo.split(" ") if len(s) > 3]


def space_count(foo: str) -> int:
    # return foo.count(" ")
    return len([s for s in foo if s == " "])


def word_size(foo: str) -> dict:
    return {w: len(w) for w in foo.split(" ")}


if __name__ == "__main__":
    print(positive([-12, 2, 5, -1, 2]))
    print(simplify([(1, 2), (3, 4)]))
    print(negate([1, 2, 3, 4]))
    print(dico([("id", 2), ("age", 20)]))
    print(to_str(["1", "2", "3"]))
    print(first_char("Je suis une phrase"))
    print(longer_than("Je suis une phrase"))
    print(space_count("Je suis une phrase"))
    print(word_size("Je suis une phrase"))

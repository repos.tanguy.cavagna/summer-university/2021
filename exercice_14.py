from typing import Callable, List, Any, TypeVar


def length(lst: List[Any]) -> int:
    cnt = 0
    for _ in lst:
        cnt += 1

    return cnt


def average(lst: List[int]) -> float:
    return sum(lst) / len(lst)


def greater_or_equal(lst: List[int], min_: int) -> List[int]:
    return [i for i in lst if i >= min_]


def less_or_equal(lst: List[int], max_: int) -> List[int]:
    return [i for i in lst if i <= max_]


def positive(lst: List[int]) -> List[int]:
    return [i for i in lst if i > 0]


def negative(lst: List[int]) -> List[int]:
    return [i for i in lst if i < 0]


def is_in(lst: List[int], target: int) -> bool:
    return next((i for i in lst if i == target), None) is not None


def minimum(lst: List[int]) -> int:
    min_ = lst[0]
    for i in lst[1:]:
        if i < min_:
            min_ = i

    return min_


def maximum(lst: List[int]) -> int:
    max_ = lst[0]
    for i in lst[1:]:
        if i > max_:
            max_ = i

    return max_


def repeat(s: str, cnt: int) -> str:
    res = ""
    for _ in range(cnt):
        res += s

    return res


def binary(s: str) -> int:
    return int(s, 2)


def and_lst(lst: List[bool]) -> bool:
    """
    for b in lst:
        if not b:
            return False
    return True
    """
    return all(lst)


def or_lst(lst: List[bool]) -> bool:
    """
    for b in lst:
        if b:
            return True
    return False
    """
    return any(lst)


def zip_(lst1: List[Any], lst2: List[Any]) -> List[tuple]:
    if len(lst1) != len(lst2):
        return []

    return [(lst1[i], lst2[i]) for i, _ in enumerate(lst1)]


def unzip(lst: List[tuple]) -> tuple:
    l1, l2 = [], []

    for a, b in lst:
        l1.append(a)
        l2.append(b)

    return l1, l2


T = TypeVar("T")
V = TypeVar("V")


def transform(lst: List[T], f: Callable[[T], V]) -> List[V]:
    return [f(i) for i in lst]


def filter(lst: List[T], validate: Callable[[T], bool]) -> List[T]:
    return [i for i in lst if validate(i)]


if __name__ == "__main__":
    print(length([1, 2, 3, 4, "awdawd"]))
    print(average([4, 5, 3]))
    print(greater_or_equal([4, 5, 12, 9, 3], 7))
    print(less_or_equal([4, 5, 12, 9, 3], 9))
    print(positive([4, -2, 3, 1]))
    print(negative([4, -2, 3, 1]))
    print(is_in([1, 2, 3], 3))
    print(minimum([3, 2, 1, 6]))
    print(maximum([3, 2, 1, 6]))
    print(repeat("slt", 5))
    print(binary("0110"))
    print(and_lst([True, True, False]))
    print(or_lst([True, True, False]))
    print(zip_([1, 2, 3], ["a", "b", "c"]))
    print(unzip([(1, "a"), (2, "b"), (3, "c")]))
    print(transform([1, 2, 3], lambda x: 2 * x + 2))
    print(filter([12, 21, 13, 45], lambda x: x > 18))

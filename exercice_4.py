def multiplication_table(x: int):
    for lhs in range(2, x + 1):
        for rhs in range(2, x + 1):
            print(f"{lhs} * {rhs} = {lhs * rhs}")

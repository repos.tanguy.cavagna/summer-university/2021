from exercice_9 import is_numeric


def ask_integer_value(message="Entrez une valeur entière") -> int:
    while value := input(f"{message} : "):
        if is_numeric(value):
            print(f"La valeur retournée est : {value}")
            break
        print("Valeur erronée")
        continue

    return int(value)


if __name__ == "__main__":
    print(ask_integer_value())

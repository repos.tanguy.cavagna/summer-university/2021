from typing import Union


def f(x: Union[int, float]) -> Union[int, float]:
    return 3 * x + 2


def g(x: Union[int, float]) -> Union[int, float]:
    return x ** 2


def h(x: Union[int, float], y: Union[int, float]) -> Union[int, float]:
    return x + 2 * y


def i(x: Union[int, float]) -> str:
    return str(x)


def j(x: Union[int, float]) -> Union[int, float]:
    return f(g(x))


def k(x: Union[int, float], y: Union[int, float]) -> str:
    return i(h(x, y))

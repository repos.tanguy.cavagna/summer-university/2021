from functools import reduce


def fibonacci():
    return reduce(lambda x, _: x + [x[-1] + x[-2]], range(9), [0, 1])


if __name__ == "__main__":
    print(", ".join(str(i) for i in fibonacci()))
